const express = require('express');
const app = express();
const port = 3123;

let lastLog = '';

function log(...messages) {
    const date = new Date();
    const dateStr = date.toLocaleString("en-US");
    const message = `${dateStr}: ${messages.join(', ')}`;
    console.log(message);
    lastLog = message;
}
//local
//localhost:3123/?ra_number=12345&redirect_url=https://miammiam.app/recipes
//localhost:3123/last-log

//prod
//https://canadapost.pierrekarampournis.website/?ra_number=12345&redirect_url=https://miammiam.app/recipes
//https://canadapost.pierrekarampournis.website/last-log
app.get('/', (req, res) => {
    const url = req.query.redirect_url;
    log(`RA Number: ${req.query.ra_number}`,
        `redirect URL: ${url}`);
    if (url) {
        res.redirect(url);
    } else {
        res.send('Requiring ra_number and redirect_url parameters.');
    }
});

app.get('/last-log', (req, res) => {
    res.send(lastLog);
});


app.listen(port, () => {
    console.log(`Canada post redirect service up at http://localhost:${port}`);
});