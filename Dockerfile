# Build the node api container
FROM node:8 AS development

# Create app directory
WORKDIR /usr/src/api

# Install api dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install --quiet
# If you are building your code for production
# RUN npm install --only=production

# Bundle api source
COPY . .

FROM node:8-slim AS production

WORKDIR /usr/src/api

COPY --from=development /usr/src/api/ ./

# start the API
EXPOSE 3123
CMD [ "npm", "start" ]

